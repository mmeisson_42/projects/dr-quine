#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int		main(void)
{
	int i = 5;
	char const * str = "#include <stdio.h>%1$c#include <string.h>%1$c#include <stdlib.h>%1$c%1$cint%2$c%2$cmain(void)%1$c{%1$c%2$cint i = %6$d;%1$c%2$cchar const * str = %3$c%5$s%3$c;%1$c%2$cchar const * prefix = %3$cSully_%3$c;%1$c%2$cchar const * new_source_fmt = %3$cSully_%%d.c%3$c;%1$c%2$cchar const * exec_fmt = %3$cSully_%%d%3$c;%1$c%2$cchar const * compile_fmt = %3$cclang -Wall -Werror -Wextra %%s -o %%s%3$c;%1$c%2$cchar const * start_fmt = %3$c./%%s%3$c;%1$c%2$cchar * source_file = NULL;%1$c%2$cchar * exec_file = NULL;%1$c%2$cchar * compile = NULL;%1$c%2$cchar * start = NULL;%1$c%2$cFILE * file;%1$c%1$c%2$cif (i < 1) { return EXIT_SUCCESS; } if (strstr(__FILE__, prefix)) { i--; }%1$c%2$casprintf(&source_file, new_source_fmt, i);%1$c%2$casprintf(&exec_file, exec_fmt, i);%1$c%2$casprintf(&compile, compile_fmt, source_file, exec_file);%1$c%2$casprintf(&start, start_fmt, exec_file);%1$c%2$cfile = fopen(source_file, %3$cw%3$c);%1$c%2$cif (file == NULL) { perror(NULL); return EXIT_FAILURE; }%1$c%2$cfprintf(file, str, '%4$cn', '%4$ct', '%3$c', '%4$c%4$c', str, i);%1$c%2$cfclose(file);%1$c%2$csystem(compile);%1$c%2$csystem(start);%1$c%2$creturn EXIT_SUCCESS;%1$c}%1$c";
	char const * prefix = "Sully_";
	char const * new_source_fmt = "Sully_%d.c";
	char const * exec_fmt = "Sully_%d";
	char const * compile_fmt = "clang -Wall -Werror -Wextra %s -o %s";
	char const * start_fmt = "./%s";
	char * source_file = NULL;
	char * exec_file = NULL;
	char * compile = NULL;
	char * start = NULL;
	FILE * file;

	if (i < 1) { return EXIT_SUCCESS; } if (strstr(__FILE__, prefix)) { i--; }
	asprintf(&source_file, new_source_fmt, i);
	asprintf(&exec_file, exec_fmt, i);
	asprintf(&compile, compile_fmt, source_file, exec_file);
	asprintf(&start, start_fmt, exec_file);
	file = fopen(source_file, "w");
	if (file == NULL) { perror(NULL); return EXIT_FAILURE; }
	fprintf(file, str, '\n', '\t', '"', '\\', str, i);
	fclose(file);
	system(compile);
	system(start);
	return EXIT_SUCCESS;
}
