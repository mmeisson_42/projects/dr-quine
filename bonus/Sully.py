#! /usr/bin/python3

import sys
import os

if __name__ == "__main__":
    i = 5
    s = r'''#! /usr/bin/python3{_NL}{_NL}import sys{_NL}import os{_NL}{_NL}if __name__ == "__main__":{_NL}{_TAB}i = {_NB}{_NL}{_TAB}s = r{_TSQ}{source}{_TSQ}{_NL}{_TAB}if i < 1:{_NL}{_TAB}{_TAB}sys.exit(0){_NL}{_TAB}if __file__.startswith("./Sully_"):{_NL}{_TAB}{_TAB}i -= 1{_NL}{_TAB}try:{_NL}{_TAB}{_TAB}with open("Sully_{{}}.py".format(i), 'w') as f:{_NL}{_TAB}{_TAB}{_TAB}print(s.format(_NL="\n", _TAB="    ", _DQ="\"", _TSQ="{_TSQ}", _NB=i, source=s), file=f){_NL}{_TAB}except Exception as e:{_NL}{_TAB}{_TAB}print(str(e), file=sys.stderr){_NL}{_TAB}else:{_NL}{_TAB}{_TAB}os.system("chmod +x {{exec}} && ./{{exec}}".format(exec="Sully_{{}}.py".format(i)))'''
    if i < 1:
        sys.exit(0)
    if __file__.startswith("./Sully_"):
        i -= 1
    try:
        with open("Sully_{}.py".format(i), 'w') as f:
            print(s.format(_NL="\n", _TAB="    ", _DQ="\"", _TSQ="'''", _NB=i, source=s), file=f)
    except Exception as e:
        print(str(e), file=sys.stderr)
    else:
        os.system("chmod +x {exec} && ./{exec}".format(exec="Sully_{}.py".format(i)))
