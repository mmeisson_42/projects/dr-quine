#! /usr/bin/python3
"""
    Ceci est un commentaire
"""

def wrap(s):
    print(s.format(_NL="\n", _TAB="    ", _DQ="\"", _TSQ="'''", source=s))

if __name__ == "__main__":
    """
        Ceci est une docstring
    """
    s = r'''#! /usr/bin/python3{_NL}"""{_NL}{_TAB}Ceci est un commentaire{_NL}"""{_NL}{_NL}def wrap(s):{_NL}{_TAB}print(s.format(_NL="\n", _TAB="    ", _DQ="\"", _TSQ="{_TSQ}", source=s)){_NL}{_NL}if __name__ == "__main__":{_NL}{_TAB}"""{_NL}{_TAB}{_TAB}Ceci est une docstring{_NL}{_TAB}"""{_NL}{_TAB}s = r{_TSQ}{source}{_TSQ}{_NL}{_TAB}wrap(s)'''
    wrap(s)
