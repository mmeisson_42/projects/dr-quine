#! /usr/bin/python3
"""
    Ceci est un commentaire
"""
import sys

DEF_FILE_NAME = "Grace_kid.py"
DEF_S = r'''#! /usr/bin/python3{_NL}"""{_NL}{_TAB}Ceci est un commentaire{_NL}"""{_NL}import sys{_NL}{_NL}DEF_FILE_NAME = "Grace_kid.py"{_NL}DEF_S = r{_TSQ}{source}{_TSQ}{_NL}{_NL}def FCT():{_NL}{_TAB}try:{_NL}{_TAB}{_TAB}with open(DEF_FILE_NAME, 'w') as f:{_NL}{_TAB}{_TAB}{_TAB}print(DEF_S.format(_NL="\n", _TAB="    ", _DQ="\"", _TSQ="{_TSQ}", source=DEF_S), file=f){_NL}{_TAB}except Exception as e:{_NL}{_TAB}{_TAB}print(str(e), file=sys.stderr){_NL}{_NL}DEF_MAIN = FCT{_NL}DEF_MAIN()'''

def FCT():
    try:
        with open(DEF_FILE_NAME, 'w') as f:
            print(DEF_S.format(_NL="\n", _TAB="    ", _DQ="\"", _TSQ="'''", source=DEF_S), file=f)
    except Exception as e:
        print(str(e), file=sys.stderr)

DEF_MAIN = FCT
DEF_MAIN()
