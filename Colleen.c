#include <stdio.h>
#include <stdlib.h>
/*
	Ceci est un commentaire
*/
int		wrap_printf(char const * str)
{
	return printf(str, '\n', '\t', '"', '\\', str);
}

int		main(void)
{
/*
	Ceci est un commentaire d'interieur
*/
	char const * str = "#include <stdio.h>%1$c#include <stdlib.h>%1$c/*%1$c%2$cCeci est un commentaire%1$c*/%1$cint%2$c%2$cwrap_printf(char const * str)%1$c{%1$c%2$creturn printf(str, '%4$cn', '%4$ct', '%3$c', '%4$c%4$c', str);%1$c}%1$c%1$cint%2$c%2$cmain(void)%1$c{%1$c/*%1$c%2$cCeci est un commentaire d'interieur%1$c*/%1$c%2$cchar const * str = %3$c%5$s%3$c;%1$c%1$c%2$cwrap_printf(str);%1$c%2$creturn EXIT_SUCCESS;%1$c}%1$c";

	wrap_printf(str);
	return EXIT_SUCCESS;
}
