
all: Sully Grace Colleen

NAME = Colleen

Sully:
	gcc -Wall -Werror -Wextra Sully.c -o Sully

Grace:
	gcc -Wall -Werror -Wextra Grace.c -o Grace

${NAME}:
	gcc -Wall -Werror -Wextra Colleen.c -o Colleen

fclean: clean

clean:
	rm -f Colleen Grace Sully

re: clean all

correction: re
	@echo
	@echo "Collen :"
	./Colleen > Colleen_tmp ; diff Colleen_tmp Colleen.c
	@echo
	@echo "Grace :"
	rm -rf Grace_kid.c ; mkdir Grace_kid.c ; ./Grace ; rm -d Grace_kid.c
	./Grace ; diff Grace.c Grace_kid.c
	@echo
	@echo "Sully: "
	rm -rf tmp; mkdir tmp ; cp Sully tmp/; cd tmp; ./Sully ; ls -al | grep Sully | wc -l ; diff ../Sully.c Sully_5.c ; diff Sully_5.c Sully_3.c ; diff Sully_3.c Sully_0.c ; cd ..
