#include <stdio.h>
#include <stdlib.h>
/*
	Ceci est un commentaire
*/
#define FILE_NAME "Grace_kid.c"
#define STR "#include <stdio.h>%1$c#include <stdlib.h>%1$c/*%1$c%2$cCeci est un commentaire%1$c*/%1$c#define FILE_NAME %3$cGrace_kid.c%3$c%1$c#define STR %3$c%5$s%3$c%1$c#define MAIN() int main(void) { FILE * file = fopen(FILE_NAME, %3$cw%3$c); if (file == NULL) return EXIT_FAILURE; fprintf(file, STR, '%4$cn', '%4$ct', '%3$c', '%4$c%4$c', STR); fclose(file); return EXIT_SUCCESS; }%1$cMAIN()%1$c"
#define MAIN() int main(void) { FILE * file = fopen(FILE_NAME, "w"); if (file == NULL) return EXIT_FAILURE; fprintf(file, STR, '\n', '\t', '"', '\\', STR); fclose(file); return EXIT_SUCCESS; }
MAIN()
